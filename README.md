# **Документация для API**

GET https://schoolspace.ru/onboarding/api/getOnboarding

Query string параметры:

```
type                      Целое число в и интервале [1, 3]
                          1 - Школьник
                          2 - Родитель
                          3 - Учитель
```

```
umnicoinBalance           Число. Баланс умникоинов пользователя
```

```
talentBalance             Число. Баланс талантов пользователя
```

```
hasPurchases              true / false. Есть ли покупки у пользователя
```

```
hasCollaborations         true / false. Есть ли коллаборации у пользователя
```

```
lastTimeVisited           Время в миллисекундах, когда пользователь последний раз зашел в приложение. 
                          0 обозначает, что пользователь зашел в первый раз
```

Пример запроса:
https://schoolspace.ru/onboarding/api/getOnboarding?type=1&umnicoinBalance=0&talentBalance=0&hasPurchases=true&hasCollaborations=false&lastTimeVisited=0

Если для пользователя не удалось подобрать сценарий, возвращается null.
В случае удачи возвращается объект с подобранным для пользователя сценарием. Формат объекта представлен ниже.

```typescript
interface Scene {
   title: string;
   pages: Page[];
   audioFile?: {
     fileName: string;
     src: string;
   }
}

interface Page {
  title: {
     value: string;
     fontSize: number;
     color: string;
     show: boolean;
   };
   text: {
     value: string;
     fontSize: number;
     color: string;
     show: boolean;
   };
   background: {
     type: string; // либо "gradient", либо "color"
     colorOne: string;
     colorTwo: string;
   };
   centerImage: {
     src: string; // Может вернуть строку "local", которая обозначает, что картинка отсутствует
     type: string; // возвращает "image"
   };
   button: {
     text: string;
     foreground: string;
     background: string;
   };
   _id: string;
}
```
